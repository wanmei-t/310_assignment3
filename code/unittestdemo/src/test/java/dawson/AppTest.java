package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void testEcho()
    {
        assertEquals("test if echo() returns the same thing as its input", 5, App.echo(5));
    }

    @Test
    public void testOneMore()
    {
        assertEquals("test if oneMore() returns one more than its input", 6, App.oneMore(5));
    }
}
